package models


import(
 	
  _ "github.com/go-sql-driver/mysql"
)

type Users struct {
    ID       int 			`sql:"type:int;not null"`
    Fname 	 string         	`sql:"type:varchar(100);not null"` // Set field as not nullable and unique
    Lname 	 string         	`sql:"type:varchar(100)"`
    Post     string 		`sql:"type:varchar(100);not null"`
}
