package controllers

import (

  //"fmt"

  _ "github.com/go-sql-driver/mysql"
  //v "github.com/spf13/viper"
  "github.com/jinzhu/gorm"

)

type DBController struct {
	DB gorm.DB
}

func (dc *DBController) InitDB() {
	var err error

 

	dc.DB, err = gorm.Open("mysql","root:12345@tcp(localhost:3306)/test?charset=utf8&parseTime=true")
	if err != nil {
		log.Fatalf("Error when connect database, the error is '%v'", err)
	}
	dc.DB.LogMode(true)
}

func (dc *DBController) GetDB() gorm.DB {
  return dc.DB
}
