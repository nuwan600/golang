package controllers
// created by H.G Nuwan Indika
import (


  "github.com/op/go-logging"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
  _ "github.com/go-sql-driver/mysql"

	//"../models"
)

var log = logging.MustGetLogger("API")

type AdsController struct {
	DB gorm.DB
}

type Users struct {
    ID       int 			`sql:"type:int;not null"`
    Fname 	 string         `sql:"type:varchar(100);not null"` // Set field as not nullable and unique
    Lname 	 string         `sql:"type:varchar(100)"`
    Post     string 		`sql:"type:varchar(100);not null"`
}

func (ac *AdsController) SetDB(d gorm.DB) {
	var err error
	ac.DB, err = gorm.Open("mysql","root:12345@tcp(localhost:3306)/test?charset=utf8&parseTime=true")
	if err != nil {
		log.Fatalf("Error when connect database, the error is '%v'", err)
	}
	ac.DB.LogMode(true)
	//ac.DB = d
	//ac.DB.LogMode(true)
}


// Get all campaigns
func (ac *AdsController) ListTest(c *gin.Context) {

	var results []Users
  err := ac.DB.Find(&results)

	if err != nil {
		log.Debugf("Error when looking up Users, the error is '%v'", err)
		res := gin.H{
				"status": "404",
				"error": "No test found",
		}
		c.JSON(404, res)
		return
	}
	content := gin.H{
			"status": "200",
            "result": "Success",
            "Users": results,
        }

  c.Writer.Header().Set("Content-Type", "application/json")
  c.JSON(200, content)
}