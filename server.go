package main

import (
	// Standard library packages

	// Third party packages
	"github.com/gin-gonic/gin"
	"./controllers"
)

func main() {


	// Get DBController instance
	dc := controllers.DBController{}
	dc.InitDB()

	// Get a AdsController instance
	ac := controllers.AdsController{}
	ac.SetDB(dc.GetDB())

	// Get a Ads resource
	router := gin.Default()

	router.GET("/test", ac.ListTest)
	router.Run(":8000")
}